﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace T02TcpClient
{
    class TcpClient
    {
        static void Main(string[] args)
        {
            //套接字
            Socket client;
            //数据缓冲池
            byte[] buf = new byte[1024];
            //输入信息
            string input;

            //1.建立套接字
            //获取远程服务器的IP地址,测试使用本机地址，实际测试可改成任意合法地址
            IPAddress remote = IPAddress.Parse("127.0.0.1");
            //端口号实际范围1~65535，其中1~1024为系统保留，也需注意避开约定成俗的常见端口 MYSql：3306 qq：4000
            IPEndPoint iep = new IPEndPoint(remote, 13000);
            //2.发送请求连接，服务端响应或拒绝或网络出现故障
            try
            {
                client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //与远程服务器建立连接
                client.Connect(iep);
            }
            catch (SocketException e)
            {
                Console.WriteLine("无法连接服务端！");
                Console.WriteLine(e.ToString());
                return;
            }
            //3.数据交换
            while (true)
            {
                //输入数据
                input = Console.ReadLine();
                if (input == "exit")
                {
                    break;
                }
                //发送数据
                client.Send(Encoding.ASCII.GetBytes(input));
                //接收应答
                int rec = client.Receive(buf);
                //输出
                Console.WriteLine(Encoding.ASCII.GetString(buf, 0, rec));
            }
            //4.释放资源
            Console.WriteLine("断开与服务器的连接......");
            client.Close();

        }
    }
}
