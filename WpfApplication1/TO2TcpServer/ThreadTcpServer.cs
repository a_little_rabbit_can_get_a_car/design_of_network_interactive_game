﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TO2TcpServer
{
    class ThreadTcpServer
    {
        //本地服务套接字
        private Socket server;
        public ThreadTcpServer()
        { 
            //1.创建套接字
            //远程的IP地址 测试使用本地回路地址
            IPAddress localAddress = IPAddress.Parse("127.0.0.1");
            IPEndPoint iep = new IPEndPoint(localAddress, 13000);
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


            //2.建立监听
            //绑定对应的IP：Port
            server.Bind(iep);
            //监听
            server.Listen(20);
            Console.WriteLine("等待客户机连接...");
            while (true)
            {
                //工作在阻塞状态
                Socket client = server.Accept();
                //创建工作线程
                ClientThread newClient = new ClientThread(client);
                Thread newThread = new Thread(new ThreadStart(newClient.handleDate));
                //启动线程
                newThread.Start();
            }
        }

       
        static void Main(string[] args)
        {
            //生成服务实例
            ThreadTcpServer instance = new ThreadTcpServer();
        }
    }
}
