﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TO2TcpServer
{
    class ClientThread
    {
        //保存客户端的连接个数
        public static int connections = 0;


        public Socket service;
        public int i;


        public ClientThread(Socket clientSocket)
        {
            this.service = clientSocket;
        }

        /// <summary>
        /// 处理服务器端与客户端之间的数据
        /// </summary>
        public void handleDate()
        {
            //数据传输：
            //1）传输的数据类型：二进制数据和文本数据
            //2）传输的数据协议：二进制协议（图片、音频、视频）文本类（JSON、XML、字符串）
            string data = null;
            //数据缓冲池
            byte[] buf = new byte[1024];

            //连接客户端的统计值
            if (service!=null)
            {
                connections++;
            }
            Console.WriteLine($"新客户端连接建立：{connections}个连接");
            while ((i=service.Receive(buf)) != 0)
            {
                data = Encoding.ASCII.GetString(buf, 0, i);
                data = data.ToUpper();
                //发送应答数据
                byte[] msg = Encoding.ASCII.GetBytes(data);
                service.Send(msg);
                Console.WriteLine($"发送数据：{data}");
            }
            //关闭与客户端的连接
            service.Close();
            connections--;
            Console.WriteLine($"客户端关闭连接：{connections}个连接数");
        }
    }
}
