﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using T01WPF;

namespace WpfApplication1
{
    /// <summary>
    /// SaveUserDialog.xaml 的交互逻辑
    /// </summary>
    public partial class SaveUserDialog : Window
    {
        //区分：新增（insert）与更新（update）
        public string ActionType { get; set; }

        private User user;

        public SaveUserDialog()
        {
            InitializeComponent();
        }

        public SaveUserDialog(User user): this ()
        { 
            this.user = user;
            Username.Text = user.Username;
            Password.Text = user.Password;
            Nickname.Text = user.Nickname;
            IsAdmin.IsChecked = user.IsAdmin;
        }

        private void SaveUser_Click(object sender, RoutedEventArgs e)
        {
            //新增
            if (ActionType=="insert") {
                User _user = new User();
                user.Username = Username.Text.Trim();
                user.Password = Password.Text.Trim();
                user.Nickname = Nickname.Text.Trim();
                user.IsAdmin = IsAdmin.IsChecked == true ? true : false;
           

            //数据插入成功
            if (UserDao.Insert(user)>0) {
                this.DialogResult = true;
                this.Close();
            }
        }
            //更新
            if (ActionType == "update")
            {
                user.Username = Username.Text.Trim();
                user.Password = Password.Text.Trim();
                user.Nickname = Nickname.Text.Trim();
                user.IsAdmin = IsAdmin.IsChecked == true ? true : false;
                UserDao.Update(user);
                this.DialogResult = true;
                this.Close();

            }
    }

    }
}
