﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using T01WPF;

namespace WpfApplication1
{
    /// <summary>
    /// FindUserDialog.xaml 的交互逻辑
    /// </summary>
    public partial class FindUserDialog : Window
    {
        public List<User> users;
        public FindUserDialog()
        {
            InitializeComponent();
        }

        private void FindUser_Click(object sender, RoutedEventArgs e)
        {
            //把框框里的东西找过来
            string strCondition = Condition.Text.Trim();
            users = UserDao.LoadData(strCondition);
            //表示窗口返回值
            this.DialogResult=true;
            this.Close();
        }
    }
}
