﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using T01WPF;

namespace WpfApplication1
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //设置DataGrid控件的数据源
            GameUserDataGrid.ItemsSource = UserDao.LoadData();
        }
        private void UserEvent_Click(object sender, RoutedEventArgs e)
        {
            //分不同时间源（sender）处理
            switch ((sender as Control).Name)
            {
                case "About":
                    Debug.WriteLine("About");
                    MessageBox.Show("这是MySQL数据访问测试程序", "关于");
                    break;
                case "Exit":
                    Debug.WriteLine("Exit");
                    this.Close();
                    Application.Current.Shutdown(0);
                    break;
                case "UserInsert":
                    Debug.WriteLine("UserInsert");
                    var insertDialog = new SaveUserDialog();
                    insertDialog.ActionType = "insert";
                    insertDialog.SaveUser.Content = "新增用户";
                    //更新
                    if (insertDialog.ShowDialog()==true) {
                        GameUserDataGrid.ItemsSource = UserDao.LoadData();
                    }
                    break;
                case "UserFind":
                    Debug.WriteLine("UserFind");
                    var findDialog = new FindUserDialog();
                    //如果对话框没有关闭，不可点击上一级窗口
                    if (findDialog.ShowDialog() == true)
                    {
                        GameUserDataGrid.ItemsSource = findDialog.users;
                    }
                    break;
                case "UserUpadate":
                    Debug.WriteLine("UserUpadate");
                    //更新的目标对象的获取
                    if (GameUserDataGrid.SelectedItem is User ) {
                        var user = GameUserDataGrid.SelectedItem as User;
                        var updateDialog = new SaveUserDialog(user);
                        updateDialog.ActionType = "update";
                        updateDialog.SaveUser.Content = "修改用户";
                        if (updateDialog.ShowDialog()==true) {
                            GameUserDataGrid.ItemsSource = UserDao.LoadData();
                        }
                    }

                    break;
                case "UserDelete":
                    Debug.WriteLine("UserDelete");
                   MessageBoxResult confrimDelete= MessageBox.Show("确认要删除所选记录吗？","提示",MessageBoxButton.YesNo,
                                                    MessageBoxImage.Question);
                    if (confrimDelete==MessageBoxResult.Yes) {
                        if (GameUserDataGrid.SelectedItem is User)
                        {
                            var user = GameUserDataGrid.SelectedItem as User;
                            UserDao.Delect(user.ID);
                            GameUserDataGrid.ItemsSource = UserDao.LoadData();
                        }
                    }
                    break;

            }
        }

        private void GameUserDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
