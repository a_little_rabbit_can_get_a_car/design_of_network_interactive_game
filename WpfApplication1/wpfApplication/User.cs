﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T01WPF
{
    /// <summary>
    /// User数据实体类；与表tb_users对应（字段）
    /// </summary>
    public class User
    {
        public long ID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nickname { get; set; }
        public bool IsAdmin { get; set; }
    
    }
}
