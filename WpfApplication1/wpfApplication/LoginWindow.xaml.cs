﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace   T01WPF
{
    /// <summary>
    /// LoginWindow1.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }
        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            string username = TxtUsername.Text.Trim();
            string password = Txtpassword.Password.Trim();
            //认证逻辑xt
            //if (username.Equals("admin") && password.Equals("admin"))//业务逻辑
                if(UserDao.LoginCheck(username,password)>0)
            {
                MessageBox.Show("登陆成功");
            }
            else {
                MessageBox.Show("用户名或密码错误!");//展现模式
     
            }
            //数据库访问：把输入的内容与数据库中的已有数据进行比较即“数据查询” SQL语句
            //数据库驱动（Driver）.Net环境中即为DLL文件
            //数据库API使用
            //合理的程序实现 三层（实体层、数据访问层、业务逻辑层）
            //实体层：数据库映射
            //数据访问层：针对具体数据库的数据操作
            //业务逻辑层：表现逻辑与业务逻辑混合


        }
    }
}
